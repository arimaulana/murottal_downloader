'use strict'

const cheerio = require('cheerio')
const fs = require('fs-extra')
const request = require('request')
const axios = require('axios')
const ProgressBar = require('progress')

const link =
	'https://islamdownload.net/124132-download-murottal-abdullah-al-mathrud.html'

let requester = link =>
	new Promise((resolve, reject) => {
		request.get(link, (err, res) => {
			if (err) {
				reject(err)
			} else {
				resolve(res.body)
			}
		})
	})

let downloader = async (filename, url, album) => {
	if (!fs.existsSync(album)) {
		fs.mkdirSync(album)
	}
	let file = fs.createWriteStream(album + '/' + filename)

    let req = request(url)
    return new Promise((resolve, reject) => {
        req.on('response', res => {
            let len = parseInt(res.headers['content-length'], 10)
            let bar = new ProgressBar(`  downloading ${filename} [:bar] :rate/bps :percent :etas`, {
                complete: '=',
                incomplete: ' ',
                width: 20,
                total: len
            })
            res.on('data', chunk => {
                bar.tick(chunk.length)
            })
            res.on('end', () => resolve())
        })
        req.end()
        req.on('error', () => reject())
    })
}

let main = async () => {
	let html = await requester(link)
	let $ = cheerio.load(html)
	let album_title = $('h2').text()
	let elements = $('table').find('tr td a')
	let data = []
	for (let idx_el = 0, length = elements.length; idx_el < length; idx_el++) {
		let element = elements[idx_el]
		let url_file = $(element).attr('href')
		let splitted_url = url_file.split('/')
		let filename = splitted_url[splitted_url.length - 1]
		data.push({ filename: filename, url: url_file, album: album_title })
	}
	// download the file
	for (let obj of data) {
		await downloader(obj.filename, obj.url, obj.album)
	}
}

main()
